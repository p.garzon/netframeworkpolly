﻿using Newtonsoft.Json;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace res.Controllers
{
    public class UserController : ApiController
    {
        public async Task<string> GetUser()
        {
            HttpResponseMessage response = null;
            String resultstring = String.Empty;
            HttpClient http = new HttpClient();
            int intentosMaximos = 5;
            var pasusaEntreReintentos = TimeSpan.FromSeconds(2);
            var url = "https://pokeapi.co/api/v2/berry/1/";
            var retryPolicy = Policy.Handle<Exception>()
                .WaitAndRetryAsync(intentosMaximos, i => pasusaEntreReintentos);


            try
            {
                await retryPolicy.ExecuteAsync(async () =>
                {
                response = await http
                .GetAsync(url);
                });
                return response.Content.ReadAsStringAsync().Result;
            }
            catch (Exception)
            {
                return null;
            }


        }

        public string GetUserByID(int id)
        {
            return "test";
        }
    }
}
